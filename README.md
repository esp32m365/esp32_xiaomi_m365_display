﻿# esp32_xiaomi_m365
Sample Project for decoding serial bus data on a Xiaomi M365 Scooter on Espressif ESP32:
- Supports one or two 0.96" 128x64 SSD1306 based OLED screen(s)
- Display connectivity via i2c or spi (spi preffered as it's much faster)
- Firmwareupdates (of the ESP32) can be done over WiFi using ArduinoOTA, Scooterfirmwareupdates are not blocked, can be done the usual way
- Connects to known WiFi Networks or opens it's own SSID in AP Mode
- optional Telnet Servers for debugging
- Config Menu for changing scooter & esp settings
- Supports different Screen Languages, Miles/Kilometers, Wheel Sizes, 10/12s batteries
- Alerts for Low Battery, ESC, BMS Temperature, Cell-Voltage Monitoring,...
- displays speeds above 32.768km/h (set for speeds from 10km/h backwards to 55,44km/h forward, backwardspeed is displayed as a positive number)

# Details
- Telnet on Port 36523 with different Screens
- Telnet on Port 36524 with raw byte dump from M365 Bus ("read only")
- Telnet on Port 36525 with packet decode/dump from M365 Bus ("read only")
- M365 Serial Receiver and Packet decoder into Array per Address
- M365 Data Requestor obey the M365 Bus Timing, does not interfere with Xiaomi's Firmware-Flash process, but can be disabled in the menu (ESP Bus Mode = Passive)
- Data Requestor internally allows subscription to predefined groups of data (e.g. OLED display only needs Cell-Voltage Data when it's displayed) to reduce requested data and therefore get a higher update rate on the interesting data fields.
- Internal Timers/Counters for debugging Receiver/Requestor & Oled-Draw/Transfer Durations - see Telnet/36523 Statistics Screen
- Different Screens for Drive/Stop/Charge/Error/Firmwareupgrade/Locked State/Timeout-Screen
- Alerts: Background Task ("Housekeeper") periodically requests additional data from M365 and validates against tresholds (set in configmenu), shows alert-popup and increases alert-counters
- Popups on screen if alerts occur, alert-counters in data screens when stopped
- Scooter can be locked/unlocked/switched off from config-menu
- Custom Screens for
	- Driving
	- "Stopped" - multiple Data screens when not moving (swipe through them using throttle)
	- "Charging" - in single-screen configuration multiple screens while charging (swipe through them using throttle)
	- Config Menu (push Brake & Throttle at the same time to enter, select items using throttle, change/enter using brake)
 	- "LOCKED" screen mode when scooter has been locked

# Todos
 - add display-sleeptimer - x seconds after last event (gas/brake/throttle/speed/chargerun-plug/telnet/AP-Client Connection & speed = 0)
 - update code to use new Adafruit Libraries which incorporated my patch ;)

# Telnet / Debugging Interface
 - Telemetrie Screen shows decoded known values: Batt Voltage, Current, Speed,... 
 - Statistics Screen dumps some internal counters, e.g. Packet Counters, CRC Files, Timing,...
 - ESC/BLE/BMS/"X1" RAW Screens dumps the 512 Byte Array for each device (format "00 00 ...")

 use the letters
  - s,t,e,b,n,x to switch between the screens
  - r to reset statistics & zero m365 data arrays

# Hardware
## Connector in M365 Headunit
M365 has a Serial Data Bus over one Wire (UART in half-duplex mode) between BLE Module and ESC (Motor Controller) and we want to connect the ESP32 to that bus, as well as get Power from the Scooter.
The Connection between BLE-Module and ESC consists of 4 wires, the connection as seen on the BLE Module:
- Ground  (Black, "G")
- One Wire Serial Connection (Yellow, "T", 115200bps, 8n1)
- VBatt (Green, "P", always available)
- 5V (Red, "5", only when scooter is turned on)

## ESP32 Powering
ESP32 needs a Vcc of 3.3V, so you can wire the 5V from M365 to a Vreg for 3.3v which powers the ESP. DO NOT POWER THE ESP32 WITH 5V!

## M365 Serial Data Bus Connection

Other then the VCC which can not be 5V, the ESP32 GPIO Pins can be connected to 5V signals.
So the Serial RX & TX Pins can be directly connected, but to ensure communication on the one-wire uart bus we need 2, to be on the safe side 3 parts:
 - 680R or 1k between RX and M365 Data Bus
 - And a Diode from M365 Data Bus in series with a ~100-200R towards TX Pin of  ESP32
That way Hardware-UART on the ESP32 can be used.

## ESP32 Pin assignment

The ESP32 has a io-switchmatrix, so (nearly) any hardwareunit can be mapped to nearly any pin.
Just be sure to not to use:
 - GPIO_NUM_34 – GPIO_NUM_39 are only GPI - Input, NO OUTPUT, No Pullup or Pulldown!
 - GPIO 6-11 are used by flash - not usable
 - GPIO 0 is for boot/flashing with pullup

# Custom PCB
I've made a PCB, see schematics folder in this repo. It has the Voltage Regulators, space for a ESP32-WROOM Module and all the small parts required for communication with the scooter. 

It supports Connection of I2C based SSD1306 display(s) usually on dev/breakoutboards OR one or 2 SSD1306 Display(s) can be soldered directly to the Connectorpads. This version will use SPI Connection and allow for a small dual-display setup.

## Bugs, possible Problem Sources
As there's a bug i've updated the schematics in this repo to fix them (v180823), for documentation here's the bugs vor v180723:

 - 1nF or 470pF C from EN towards GND and 12-18k R from EN towards 3.3V are missing (sad copy and paste error)
 - the SOT23 Diode housing might be wrong - depending on your Diode (BAS70xx versions) -> the Diode-Package can be rotated 120° CCW to fix that

I have some spare PCBs of v180723 (with the bug), if you know how to solder small pads,... i can send you that version. If you want to get a idea how the fix is applied there's pictures in the schematics folder.

For applying the fix you need:
 - 1nF or 470pF C (C17 in v180823) from EN towards GND
 - 12-18k R (R9 in v180823) from EN towards 3.3V
Please see JPG in schematics folder to get an idea where to place C17 (blue line) and R9 (yellow line)

I'm not gonna make a new batch after that for the fixed version, as i'm already working on a PCB with different features.
## Build using Breakout Boards
As a sugesstion for building a display using breakout boards which need just some added parts, here's recommendations from my first test-devices:
 * ESP32 Dev Board
   * e.g. [ESP32 Mini D1](https://www.aliexpress.com/wholesale?SearchText=esp32+mini+d1)
   * Connect GND to "G", VCC to "5"
   * You will need 2 resistors and a diode, please refer to GPIO Pins IO22 & IO23 in the schematics, the M365BUS wire connects to "T"
   * "G", "5", "T" are the labels on the BLE-Module next to the powerswitch
 * SSD1306 OLED Board(s)
   * e.g. [SSD1306 128x64 0.96](https://www.aliexpress.com/wholesale?SearchText=ssd1306+128x64+0.96)
   * Connect it with GND and 3.3V
   * SDA/SCL should have Pullup Pins on the Breakout board, when using 2 display on the same bus, one breakoutboard should have the Pullups disabled or removed (though you can try with both...)

# Building your own Display
## Compiling
This project uses [PlatformIO](https://platformio.org/), please check out their [Documentation](https://docs.platformio.org/en/latest/) to get started.
This Project can also be compiled using the Arduino-IDE, though the needed libraries have to be installed manually - therefore platformio is recommended.
## Setup individual settings
### SSID's and passwords:
 - please open secrets.h and fill in your own ssid/passwords for client/ap mode and ota updates!
### Software configuration for your hardware
 - copy one of my board*.h files that fits your hardware best, update it for your setup and include this file in definitions.h
 - when updating my code you just need to reapply the include of your boards file in definitions.h
## Choose your environment  (platformio):
  - PlatformIO supports different (build-)"Environments", you will mostly want to select "m365client" which implements the "client" for a stock m365 scooter and the oled display.
## PATCH your libraries!
  - Plattformio includes my patched Adafuit_SSD1306 library for you, if u are using a different ide, please look in platformio.ini for references to the patches libraries and make sure those are used. In the meanwhile Adafruit updated it's libraries to get rid of the problems, so one day i'll update my code to use the new lib from Adafruit and get rid of the patched libraries.
  - earlier code versions needed some files patched in arduino-esp32 framework in order for correct uart functionality. now i've included the patched UART-Files in this repo, so the patch in arduino-esp32 is not needed anymore (save you time and solves the problem that your other projects would also use that patched framework). But one day when arduino-esp32 framework get's updated this might brake and the files included in this project need to be updated.
  - unfortunately the latest update of platform-io included the arduino-esp32 framework with a broken i2c implementation. This needs to be patched manually, but it fixes a general problem, so this will help your other projects using i2c as well. For patching please see this issue and replace the 4 files as mentioned - otherwise the i2c-display will hang at most restarts: https://github.com/platformio/platform-espressif32/issues/126

## Programming the ESP
### Wired
The ESP is programmed over a Serial Connection, either your ESP-Board already has a USB Connector or you need to setup the wiring according to this [Espressif's Documentation](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/establish-serial-connection.html)
### OTA/Wireless programming
After the 1st programming the ESP32 can be programmed via WiFi network, in platformio it depends on the IDE you are using - e.g. Sublime Text with DevIOT Package has a menu for selecting a device (entering the IP Address), VSCode lacks that function -> open platformio.ini and under the m365client environment add `upload_port = <ip>` (ofc <ip> is replaced by the IP Adress the ESP gives you on the display / asset screen or on the serial debug terminal).
Here you will also have to provide the OTA-password you have set in secrets.h

## First startup
After flashing the ESP32 you will have a Serial UART Connection, just start a terminal with 115200bps and you will see some debug messages.
The ESP will search for a WIFI, if it does not find one, it will start a Access-Point.
Before Connecting your ESP32 with the scooter, make shure you see a splashscreen on the screen and a DATA TIMEOUT Error message - otherwise your GPIO/Display Setup in your boards file is wrong (or you are not including your boards file from definitions.h?)
Once you see something on the display you can connect the scooter.

**Attention** Don't power your ESP from your Computer (USB...) and the Scooter at the same time. Either power it from your Computer and only Connect "G" and "T" or connect "5" but do not Connect 5V from your Computer (which will not be easy when you have a USB-Port on your dev-board).

When you've wired the ESP32 to the Scooter and turn on the scooter you should see the splash screen followed by a data screen that will show you e.g. the battery level. 

If you pull the throttle now, the ESP should switch to different screens. If that does not happen, the ESP32 has no UART Connection. check your GPIOs, resistors/diode and cabling.

If there's a response on the screen to pushing throttle, you already know how to switch screens - now please check the battery level, if it reads 0% your ESP can not _send_ data on the bus -> Check your TX Pin GPIO, Diode, Resistors, Wiring...

If the Battery Level is shown, everythings fine - Congratulations!

# Support, Questions,...?
 - Telegram Group https://t.me/esp32brain4m365

# Credits & Thanks to
 - Paco Gorina for his research on the protocoll and packet structure (-> http://www.gorina.es/9BMetrics/)
 - CamiAlfa for his research and providing informations about the contents of the packets & arrays (-> https://github.com/CamiAlfa/M365-BLE-PROTOCOL)
 - french translation kindly provided by Technoo' Loggie (Telegram Handle @TechnooLoggie)

# Licensed under Creative Commons V4 - BY NC SA
 - THIS MEANS YOU CAN USE THIS CODE FOR NON COMMERCIAL PURPOSES, HAVE TO RESHARE YOUR MODIFICATIONS AND REFERENCE THIS REPO.